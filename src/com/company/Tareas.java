package com.company;

/**
 * Created by brayan.jimenez on 17/05/2017.
 */
public interface Tareas {
    void pagarServicios();
    void contratarPersonal();
    void pagarProveedores();

}

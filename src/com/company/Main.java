package com.company;

import javax.swing.*;

public class Main {

    public static void main(String[] args) {
        // TODO Auto-generated method stub

        int opcion;
        int sw;
        String salida = "";
        int numEmpleados;
        int i=0;

        numEmpleados = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el numero de empleados : "));

        Empleado [] listEmpleado = new Empleado[numEmpleados];

        do {
            opcion = Integer.parseInt(JOptionPane.showInputDialog("Menú :\n1. Empleado Asalariado.\n2. Empleado Comision\n3. Empleado Horas\n4. Empleado Base Comision"));

            switch(opcion) {

                case 1:
                    listEmpleado[i] = new Asalariado();
                    i++;
                    break;

                case 2:
                    listEmpleado[i] = new Comision();
                    i++;
                    break;

                case 3:
                    listEmpleado[i] = new Horas();
                    i++;
                    break;
                case 4:
                    listEmpleado[i] = new BaseComision();
                    i++;
                    break;
            }
            sw = JOptionPane.showConfirmDialog(null, "Desea probar con otros empleados....;","OPCION",JOptionPane.YES_NO_OPTION);
        }
        while(sw == JOptionPane.YES_OPTION);

        for(int z=0; z<i ; z++) {
            salida+= listEmpleado[z].titulo() + listEmpleado[z] + "\nA Pagar : " + listEmpleado[z].getSueldo() + "$\n";
        }
        JOptionPane.showMessageDialog(null, salida);
    }


}
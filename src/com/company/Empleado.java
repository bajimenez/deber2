package com.company;

import javax.swing.*;

/**
 * Created by brayan.jimenez on 17/05/2017.
 */
public abstract class Empleado {
    private String nombre;
    public abstract double getSueldo();
    public abstract String titulo();

    //Constructor
    public Empleado() {
        super();

        this.nombre = JOptionPane.showInputDialog("Ingrese el nombre : ");
    }

    public Empleado(String nombre){
        this.nombre= nombre;

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String toString() {
        String salida = "";

        salida+="Nombre : "  + this.nombre;

        return salida;
    }
    public enum cargo{
        gerente,subgerente,asesor,contable;
    }
    cargo carg;
    public void DetallesCargo(){
        switch (carg){

            case asesor:
                System.out.print("Guiar y proponer ideas a la gerencia");
                break;

            case gerente:
                System.out.print("Dirige, gestiona y toma las decisiones");
                break;

            case subgerente:
                System.out.print("");
                break;
        }
    }
}

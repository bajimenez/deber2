package com.company;

import javax.swing.*;

/**
 * Created by brayan.jimenez on 17/05/2017.
 */
public class Asalariado extends Empleado implements Tareas {  private double salarioBasico;

    //Constructor
    public Asalariado() {
        super();

        this.salarioBasico = Double.parseDouble(JOptionPane.showInputDialog(null, "Ingrese el salario basico : ", "Empleado Asalariado", JOptionPane.DEFAULT_OPTION));
    }



    public double getSueldo() {
        return this.salarioBasico;
    }

    public String titulo() {
        return ("\nEMPLEADO ASALARIADO\n");
    }

    public String toString() {
        String salida = "";

        salida+=super.toString();
        salida+="\nSalario Basico : " + this.salarioBasico + "$";

        return salida;
    }

    @Override
    public void pagarServicios() {

    }

    @Override
    public void contratarPersonal() {

    }

    @Override
    public void pagarProveedores() {

    }
}
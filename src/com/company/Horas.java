package com.company;

import javax.swing.*;

/**
 * Created by brayan.jimenez on 17/05/2017.
 */
public class Horas extends Empleado{
    private int horasTrabajadas;
    private double costoHoras;
    public  Horas(){
    super();

        this.horasTrabajadas = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingrese las horas trabajadas : ", "Empleado Horas", JOptionPane.DEFAULT_OPTION));
        this.costoHoras = Double.parseDouble(JOptionPane.showInputDialog(null, "Ingrese el costo de horas : ", "Empleado Horas", JOptionPane.DEFAULT_OPTION));
}

    public Horas (int horasTrabajadas, double costoHoras){
        this.horasTrabajadas=horasTrabajadas;
        this.costoHoras= costoHoras;

    }


    public int getHorasTrabajadas() {
        return horasTrabajadas;
    }

    public void setHorasTrabajadas(int horasTrabajadas) {
        this.horasTrabajadas = horasTrabajadas;
    }
    public void setHorasTrabajadas(String horasTrabajadas) {
        this.horasTrabajadas = Integer.valueOf(horasTrabajadas);
    }

    public double getCostoHoras() {
        return costoHoras;
    }

    public void setCostoHoras(double costoHoras) {
        this.costoHoras = costoHoras;
    }

    public double getSueldo() {
        return (this.horasTrabajadas * this.costoHoras);
    }

    public String titulo() {
        return ("\nEMPLEADO HORAS\n");
    }

    public String toString() {
        String salida = "";

        salida+=super.toString();
        salida+="\nHoras Trabajadas : " + this.horasTrabajadas + "H";
        salida+="\nCosto Horas : " + this.costoHoras + "$";

        return salida;
    }
}

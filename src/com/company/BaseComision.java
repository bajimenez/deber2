package com.company;

import javax.swing.*;

/**
 * Created by brayan.jimenez on 17/05/2017.
 */
public class BaseComision extends Comision { private double salarioBase;

    //Constructor
    public BaseComision() {
        super();

        this.salarioBase = Double.parseDouble(JOptionPane.showInputDialog(null, "Ingrese el salario base : ", "Empleado Base comision", JOptionPane.DEFAULT_OPTION));
    }

    public double getSalarioBase() {
        return salarioBase;
    }

    public void setSalarioBase(double salarioBase) {
        this.salarioBase = salarioBase;
    }

    public double getSueldo() {
        return (this.salarioBase + super.getSueldo());
    }

    public String titulo() {
        return ("\nEMPLEADO BASE COMISION\n");
    }

    public String toStirng() {
        String salida = "";

        salida+=super.toString();
        salida+="\nSalario Base : " + this.salarioBase + "$";

        return salida;
    }

    @Override
    public Double calculoComision(double porcentajeComision) {
        double total=0;
        total = porcentajeComision*0.12;
        return total;
    }
}
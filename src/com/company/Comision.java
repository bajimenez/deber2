package com.company;

import javax.swing.*;

/**
 * Created by brayan.jimenez on 17/05/2017.
 */
public class Comision extends Empleado {
    private double totalVendido;
    private double porcentajeComision;

    //Constructor
    public Comision() {
        super();

        this.totalVendido = Double.parseDouble(JOptionPane.showInputDialog(null, "Ingrese el total vendido : ", "Empleado Comision", JOptionPane.DEFAULT_OPTION));
        this.porcentajeComision = Double.parseDouble(JOptionPane.showInputDialog(null, "Ingrese el porcentaje comision : ", "Empleado Comision", JOptionPane.DEFAULT_OPTION));
    }

    public Comision(double totalVendido, double porcentajeComision){
        this.totalVendido=totalVendido;
        this.porcentajeComision= porcentajeComision;    }

    public double getTotalVendido() {
        return totalVendido;
    }

    public void setTotalVendido(double totalVendido) {
        this.totalVendido = totalVendido;
    }

    public double getPorcentajeComision() {
        return porcentajeComision;
    }

    public void setPorcentajeComision(double porcentajeComision) {
        this.porcentajeComision = porcentajeComision;
    }

    public double getSueldo() {
        return (this.totalVendido * (this.porcentajeComision/100));
    }

    public String titulo() {
        return ("\nEMPLEADO COMISION\n");
    }

    public String toString() {
        String salida = "";

        salida+=super.toString();
        salida+="\nTotal Vendido : " + this.totalVendido + "$";
        salida+="\nPorcentaje Comision : " + this.porcentajeComision + "%";

        return salida;
    }
    public Double calculoComision(double porcentajeComision){
        return totalVendido*(porcentajeComision/100);
    }
    public Double calculoComision (String porcentajeComision){
        return totalVendido*(Double.valueOf(porcentajeComision)/100);

    }
}